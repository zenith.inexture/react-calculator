import React from "react";
import "./custom.css";

class Calculator extends React.Component {
  constructor() {
    super();
    this.state = {
      value: "", //display of calculator
      array: [7, 8, 9, 4, 5, 6, 1, 2, 3, 0], //create array for random display
    };
    this.handleClick = this.handleClick.bind(this); //bind onclick method
  }

  //random function  -  Create random sorted array
  random = (prearray) => {
    this.setState({
      array: prearray.sort(function () {
        return 0.5 - Math.random();
      }),
    });
  };

  //onclick handleclick function
  handleClick(event) {
    if (event.target.value >= 0 && event.target.value <= 9) {
      this.random(this.state.array);
    }
    if (event.target.value === "=") {
      this.calculate();
    } else if (event.target.value === "Clear") {
      this.clear();
    } else if (event.target.value === "Delete") {
      this.delete();
    } else {
      debugger;
      if (this.state.value === "Error") {
        this.setState({ value: event.target.value });
      } else {
        this.setState({
          value: this.state.value + event.target.value,
        });
      }
    }
  }

  //caluculate function trigger when user click =
  calculate = () => {
    try {
      var ans = eval(this.state.value);
      this.setState({ value: ans });
    } catch (err) {
      this.setState({ value: "Error" });
    }
  };

  //clear function - clear the value
  clear = () => {
    this.setState({ value: "" });
  };

  //delete function - delete last elements
  delete = () => {
    if (this.state.value === "Error") {
      this.setState({ value: "" });
    } else {
      var str = this.state.value.slice(0, -1);
      this.setState({ value: str });
    }
  };

  //render in html file
  render() {
    return (
      <div className="maindiv">
        <h2 className="title">CalculatoR</h2>
        <div className="mainCalc">
          <h3>{this.state.value}</h3>
          <div className="button-row-fst button-row">
            <input type="button" value="(" onClick={this.handleClick} />
            <input type="button" value="Delete" onClick={this.handleClick} />
            <input type="button" value=")" onClick={this.handleClick} />
            <input type="button" value="Clear" onClick={this.handleClick} />
          </div>
          <div className="button-row">
            <input
              type="button"
              value={this.state.array[0]}
              onClick={this.handleClick}
            />
            <input
              type="button"
              value={this.state.array[1]}
              onClick={this.handleClick}
            />
            <input
              type="button"
              value={this.state.array[2]}
              onClick={this.handleClick}
            />
            <input type="button" value="+" onClick={this.handleClick} />
          </div>
          <div className="button-row">
            <input
              type="button"
              value={this.state.array[3]}
              onClick={this.handleClick}
            />
            <input
              type="button"
              value={this.state.array[4]}
              onClick={this.handleClick}
            />
            <input
              type="button"
              value={this.state.array[5]}
              onClick={this.handleClick}
            />
            <input type="button" value="-" onClick={this.handleClick} />
          </div>
          <div className="button-row">
            <input
              type="button"
              value={this.state.array[6]}
              onClick={this.handleClick}
            />
            <input
              type="button"
              value={this.state.array[7]}
              onClick={this.handleClick}
            />
            <input
              type="button"
              value={this.state.array[8]}
              onClick={this.handleClick}
            />
            <input type="button" value="*" onClick={this.handleClick} />
          </div>
          <div className="button-row-lst button-row">
            <input type="button" value="." onClick={this.handleClick} />
            <input
              type="button"
              value={this.state.array[9]}
              onClick={this.handleClick}
            />
            <input type="button" value="=" onClick={this.handleClick} />
            <input type="button" value="/" onClick={this.handleClick} />
          </div>
        </div>
      </div>
    );
  }
}

export default Calculator;
