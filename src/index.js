import React from 'react';
import ReactDOM from 'react-dom';
import Calculator from './Calculator.js';
  
// Render the Calculator to the Web page.
ReactDOM.render(<Calculator />, document.getElementById('root'));
